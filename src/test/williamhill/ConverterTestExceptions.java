package test.williamhill;

import main.williamhill.Converter;
import main.williamhill.RacePrice;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(Parameterized.class)
public class ConverterTestExceptions {
    private BigDecimal totalPriceLimit;

    public ConverterTestExceptions(BigDecimal totalPriceLimit) {
        this.totalPriceLimit = totalPriceLimit;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testConditions(){
        return Arrays.asList(new Object[][]{
                {null},
                {new BigDecimal("-6")}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwsException() {
        //Test case when totalPriceLimit is null
        Converter.specialOfferConverter(new RacePrice(new BigDecimal("10"), Arrays.asList(new BigDecimal("5"), new BigDecimal("5"))), totalPriceLimit);

        //Test case when totalPriceLimit == negative value
        Converter.specialOfferConverter(new RacePrice(new BigDecimal("10"), Arrays.asList(new BigDecimal("5"), new BigDecimal("5"))), totalPriceLimit);

        //Test case when basePrice is null
        Converter.specialOfferConverter(new RacePrice(null, Arrays.asList(new BigDecimal("5"), new BigDecimal("5"))), new BigDecimal("10"));

        //Test case when one of the taxes is null
        Converter.specialOfferConverter(new RacePrice(new BigDecimal("9"), Arrays.asList(null, new BigDecimal("1"), new BigDecimal("1"))), new BigDecimal("6"));
    }
}