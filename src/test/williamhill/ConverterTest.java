package test.williamhill;

import main.williamhill.Converter;
import main.williamhill.RacePrice;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(Parameterized.class)
public class ConverterTest {
    private RacePrice inputObj;
    private RacePrice expectedObj;
    private BigDecimal totalPriceLimit;

    public ConverterTest(RacePrice inputObj, RacePrice expectedObj, BigDecimal totalPriceLimit) {
        this.inputObj = inputObj;
        this.expectedObj = expectedObj;
        this.totalPriceLimit = totalPriceLimit;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testConditions(){
        return Arrays.asList(new Object[][]{
                {new RacePrice(new BigDecimal("10"), Arrays.asList(new BigDecimal("5"), new BigDecimal("5"))), new RacePrice(new BigDecimal("5.00"), Arrays.asList(new BigDecimal("2.50"), new BigDecimal("2.50"))), new BigDecimal("10")},
                {new RacePrice(new BigDecimal("9"), Arrays.asList(new BigDecimal("1"), new BigDecimal("1"), new BigDecimal("1"))), new RacePrice(new BigDecimal("4.50"), Arrays.asList(new BigDecimal("0.50"), new BigDecimal("0.50"), new BigDecimal("0.50"))), new BigDecimal("6")}
        });
    }

    @Test
    public void verifierSpecialOfferConverter(){
        assertEquals(expectedObj, Converter.specialOfferConverter(inputObj, totalPriceLimit));
    }
}