package main.williamhill;

import java.math.BigDecimal;
import java.util.List;

import static java.util.stream.Collectors.*;

public class Converter {

    //This function will convert RacePrice to meet the Gaming company total price limit
    public static RacePrice specialOfferConverter(RacePrice obj, BigDecimal totalPriceLimit) {
        if (totalPriceLimit == null) {
            throw new IllegalArgumentException("Total price can't be null");
        } else if (totalPriceLimit.doubleValue()>=0.0) {
            if (totalPriceLimit.doubleValue() == 0.0) {
                return new RacePrice(calculateBasePrice(obj, totalPriceLimit), calculateTaxes(obj, totalPriceLimit));
            } else {
                BigDecimal rate = totalPriceLimit.divide(obj.countTotalPrice());
                return new RacePrice(calculateBasePrice(obj, rate), calculateTaxes(obj, rate));
            }
        }
        throw new IllegalArgumentException("Total price can't be negative value");
    }

    private static List<BigDecimal> calculateTaxes(RacePrice obj, BigDecimal rate) {
        return obj.getTaxesList().stream().map(x -> x.multiply(rate)).collect(toList());
    }

    private static BigDecimal calculateBasePrice(RacePrice obj, BigDecimal rate) {
        return obj.getBasePrice().multiply(rate);
    }
}
