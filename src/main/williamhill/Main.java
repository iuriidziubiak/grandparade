package main.williamhill;

import java.math.BigDecimal;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        RacePrice horseRace = new RacePrice(new BigDecimal("10"), Arrays.asList(new BigDecimal("5"), new BigDecimal("5")));

        RacePrice converted = Converter.specialOfferConverter(horseRace, new BigDecimal("10"));

        System.out.println(converted.getBasePrice());
        System.out.println(converted.getTaxesList());
    }
}
