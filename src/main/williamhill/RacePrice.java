package main.williamhill;

import java.math.BigDecimal;
import static java.util.stream.Collectors.*;
import java.util.List;
import java.util.Objects;

public class RacePrice {

    private BigDecimal basePrice;
    List<BigDecimal> taxesList;

    public RacePrice(BigDecimal basePrice, List<BigDecimal> taxesList) {
        setBasePrice(basePrice);
        setTaxesList(taxesList);
    }

    private void checkTaxes(List<BigDecimal> taxesList) {
        for (BigDecimal tax: taxesList) {
            if(tax == null) {
                throw new IllegalArgumentException("Taxes can't be null");
            } else if (tax.doubleValue() < 0) {
                throw new IllegalArgumentException("Taxes can't be negative");
            }
        }
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        if(basePrice != null) {
            if (basePrice.doubleValue() < 0) {
                throw new IllegalArgumentException("Base price can't be negative");
            } else {
                this.basePrice = basePrice.setScale(2);
            }
        } else throw new IllegalArgumentException("Base price can't be null");
    }

    public List<BigDecimal> getTaxesList() {
        return taxesList;
    }

    public void setTaxesList(List<BigDecimal> taxesList) {
        checkTaxes(taxesList);
        this.taxesList = taxesList.stream().map(x -> x.setScale(2)).collect(toList());
    }

    //This function calculate the total price
    public BigDecimal countTotalPrice() {
        BigDecimal totalPrice = new BigDecimal("0");
        for (BigDecimal tax: taxesList) {
            totalPrice=totalPrice.add(tax);
        }
        return totalPrice.add(basePrice);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RacePrice)) return false;
        RacePrice racePrice = (RacePrice) o;
        return basePrice.equals(racePrice.basePrice) &&
                Objects.equals(taxesList, racePrice.taxesList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(basePrice, taxesList);
    }
}
